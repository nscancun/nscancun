import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertLoginService {

  constructor(public alertC: AlertController) { }

  async accesoCorrectoAlert() {
    const alert = await this.alertC.create({
      header: 'Felicidades!!',
      // backdropDismiss: false,
      message: 'Acceso Correcto',
      buttons: ['OK']
    });
    await alert.present();
  }

  async creacionCorrectaAlert() {
    const alert = await this.alertC.create({
      header: 'Felicidades!!',
      // backdropDismiss: false,
      message: 'Cuenta creada',
      buttons: ['OK']
    });
    await alert.present();
  }

  async errorAlert(mensaje) {
    const alert = await this.alertC.create({
      header: 'Error!!!',
      // backdropDismiss: false,
      message: mensaje,
      buttons: ['OK']
    });
    await alert.present();
  }
}
