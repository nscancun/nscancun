import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
// import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../shared/user.class';
import { AlertLoginService } from './alert-login.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public isLogged: any = false;
  id: string;
  prueba;
  tasksRef: AngularFireList<any>;

  constructor(
    // public afAuth: AngularFireAuth,
    private alert: AlertLoginService,
    private router: Router,
    private afAuth: AngularFireAuth,
    private al: AlertLoginService,
    private database: AngularFireDatabase,
    ) {
    this.tasksRef = this.database.list('/Usuarios');
    afAuth.authState.subscribe(user => (this.isLogged = user));
  }

  // Login
  async onLogin( user: User ) {
    firebase.auth().signInWithEmailAndPassword(
      user.email,
      user.password);
    try {
      return await this.afAuth.auth.signInWithEmailAndPassword(
        user.email,
        user.password);
    } catch (error) {
      console.log('Error al iniciar sesión', error);
      this.alert.errorAlert(error.message);
    }
  }

  // Registro
  async onRegister( user: User ) {
    try {
      return await this.afAuth.auth.createUserWithEmailAndPassword(
        user.email,
        user.password).then((res) => {
          this.afAuth.auth.signInWithEmailAndPassword(
            user.email,
            user.password).then((ress) => {
              let user = firebase.auth().currentUser;
              user.updateProfile({
                displayName: "Reportero Anonimo",
                photoURL: "https://cdn.pixabay.com/photo/2014/06/27/16/47/person-378368_960_720.png"
              });

              this.tasksRef.update(user.uid, {
                idUsuario: user.uid,
                Nombre: "Usuario Anonimo",
                Apellidos: '',
                photoURL: "https://cdn.pixabay.com/photo/2014/06/27/16/47/person-378368_960_720.png",
                correo: user.email,
              });

              console.log('Creado correctamente');
              this.al.creacionCorrectaAlert();
              this.router.navigateByUrl('/home');
            });
        });
    } catch (error) {
      console.log('Error al iniciar sesión', error);
      this.alert.errorAlert(error.message);
    }
  }

  logout() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigateByUrl('/');
    });
  }
  
}
