import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotiServiceService {

  url = 'http://newsapi.org/v2/top-headlines?' +
  'country=mx&category=business&' +
  'apiKey=9ea5cffb0f13479395378c2d5f6c0be1';

  constructor(
    private http: HttpClient
  ) { }

  getNoticias() {
    return new Promise(resolve => {
      this.http.get(this.url).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

}
