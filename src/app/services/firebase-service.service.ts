import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class FirebaseServiceService {
  private basePath: string = '/Noticias';

  posts: AngularFireList<any> = null; //  list of objects
  post: any = null; //   single object

  constructor(
    private db: AngularFireDatabase,
    ) { }

  getNoticiaList(): any {
    this.posts = this.db.list(this.basePath);
    return this.posts;
  }
}
