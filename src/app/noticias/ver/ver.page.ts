import { Component, OnInit } from '@angular/core';
import * as firebase from 'Firebase';
import { ActivatedRoute, Router  } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { LoadingService } from '../../services/loading.service';
import { AlertasService } from '../../services/alertas.service';

@Component({
  selector: 'app-ver',
  templateUrl: './ver.page.html',
  styleUrls: ['./ver.page.scss'],
})
export class VerPage implements OnInit {
  id = null;
  idAutor;
  noticia = {};
  autor = {};
  userRef: AngularFireList<any>;
  statusFav;

  constructor(
    private route: ActivatedRoute,
    public router: Router,
    public afd: AngularFireDatabase,
    private alertasSvc: AlertasService,
    private loadingSvc: LoadingService,
  ) {  }

  ngOnInit() {
    this.loadingSvc.cargando();
    this.id = this.route.snapshot.paramMap.get('key');
    const data = this.afd.object('/Noticias/' + this.id).valueChanges().subscribe(val => {
      this.noticia =  val;
      this.obtenerInfoAutor();
    });
  }

  obtenerInfoAutor() {
    const idd = this.afd.object('/Noticias/' + this.id + '/idUsuario').valueChanges().subscribe(val => {
      this.idAutor =  val;
      this.userRef = this.afd.list('/Usuarios');
      const data = this.afd.object('/Usuarios/' + this.idAutor).valueChanges().subscribe( valo => {
        this.autor =  valo;
        this.verficarFav();
      });
    });
  }

  // Guarda la noticia a Favoritos del usuario
  addFavoritos() {
    let User = firebase.auth().currentUser;
    const favRef = this.afd.list('/Usuarios/' + User.uid + '/favoritos');
    favRef.update(this.id, {
      favId: this.id
    });
    this.alertasSvc.addFavAlert();
  }
  // Elimina la noticia de Favoritos
  deleteFav() {
    let User = firebase.auth().currentUser;
    const favRef = this.afd.list('/Usuarios/' + User.uid + '/favoritos/');
    favRef.remove(this.id);
    this.alertasSvc.deleteFavAlert();
  }
  // Verifica si esta noticia la tiene en Favoritos
  verficarFav() {
    let User = firebase.auth().currentUser;
    const data = this.afd.object('/Usuarios/' + User.uid + '/favoritos/' + this.id).valueChanges().subscribe(val => {
      if(val) {
        this.statusFav = false;
        console.log('yes');
      } else {
        this.statusFav = true;
        console.log('No');
      }
      this.loadingSvc.dismiss();
    });
  }

  verAutor() {
    // this.idAutor
    this.router.navigate(['/otrousuario/' + this.idAutor]);
  }

}
