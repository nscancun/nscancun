import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize, delay } from 'rxjs/operators';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray } from '@angular/forms';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AlertasService } from '../../services/alertas.service';
import { LoadingService } from '../../services/loading.service';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {
  ref = firebase.database().ref('Noticias/');
  notiForm: FormGroup;
  id;idIMG;
  img;img_new;
  image: any;
  file: any;
  urlImage: Observable<string>;
  itemsRef: AngularFireList<any>;

  constructor(
    private route: ActivatedRoute,
    public router: Router,
    public afd: AngularFireDatabase,
    private formBuilder: FormBuilder,
    private alertasSvc: AlertasService,
    private loadingSvc: LoadingService,
    private storage: AngularFireStorage,
  ) {
    this.notiForm = this.formBuilder.group({
      'titulo' : [null, Validators.required],
      'noticia' : [null, Validators.required]
    });

    this.id = this.route.snapshot.paramMap.get('key');
    this.getInfo(this.route.snapshot.paramMap.get('key'));
  }

  ngOnInit() {
    this.itemsRef = this.afd.list('/Noticias');
  }

  getInfo(key) {
    firebase.database().ref('Noticias/' + key).on('value', resp => {
      let info = snapshotToObject(resp);
      this.notiForm.controls['titulo'].setValue(info.titulo);
      this.notiForm.controls['noticia'].setValue(info.noticia);
      this.img = info.urlIMG;
      this.idIMG = info.idIMG;
      console.log(this.idIMG);
    });
  }

  updateInfo() {
    let newInfo = firebase.database()
      .ref('Noticias/' + this.route.snapshot.paramMap.get('key'))
      .update(this.notiForm.value);

    this.itemsRef.update(this.id, this.notiForm.value);
    console.log('key', this.id);
    console.log('value', this.notiForm.value);
    this.alertasSvc.updateNotiAlert();
    // this.router.navigate(['/vernoticias']);
  }

  // Obtine la imagen que abrimos
  onUpload(e) {
    this.file = e.target.files[0];
    this.subirIMG();
    this.previewImg(e);
    console.log('subir', e.target.files[0]);
  }
  // Muestra la imagen elegida al usuario
  previewImg(e) {
    let reader = new FileReader();
    reader.onload = (e:any) => {
      this.img_new = e.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
    this.img = '';
  }

  // Sube la imagen a firebase
  subirIMG() {
    const filePath = 'noticias/' + this.idIMG ;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload((filePath), this.file);

    this.loadingSvc.cargaIMG();
    task.snapshotChanges().pipe(
      finalize(() => {
        this.urlImage = ref.getDownloadURL();
        this.loadingSvc.dismiss();
      })).subscribe();
    console.log('url img', this.urlImage);
  }

}

export const snapshotToObject = snapshot => {
  let item = snapshot.val();
  item.key = snapshot.key;

  return item;
}
