import { Component, OnInit } from '@angular/core';
import { NotiServiceService } from '../../services/noti-service.service';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-notimexico',
  templateUrl: './notimexico.page.html',
  styleUrls: ['./notimexico.page.scss'],
})
export class NotimexicoPage implements OnInit {

  noticias: any;

  constructor(
    public notiService: NotiServiceService,
    private loadingSvc: LoadingService,
  ) { }

  ngOnInit() {
    this.loadingSvc.cargando();
    this.getNoticias();
  }


  getNoticias() {
    this.notiService.getNoticias().then(data => {
      this.noticias = data['articles'];
      this.loadingSvc.dismiss();
    });
  }

}
