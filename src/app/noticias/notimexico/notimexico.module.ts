import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotimexicoPageRoutingModule } from './notimexico-routing.module';

import { NotimexicoPage } from './notimexico.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotimexicoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [NotimexicoPage]
})
export class NotimexicoPageModule {}
