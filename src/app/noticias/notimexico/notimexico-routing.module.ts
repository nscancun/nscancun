import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotimexicoPage } from './notimexico.page';

const routes: Routes = [
  {
    path: '',
    component: NotimexicoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotimexicoPageRoutingModule {}
