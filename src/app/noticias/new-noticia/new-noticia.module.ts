import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewNoticiaPageRoutingModule } from './new-noticia-routing.module';

import { NewNoticiaPage } from './new-noticia.page';
import { ComponentsModule } from '../../components/components.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewNoticiaPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [NewNoticiaPage]
})
export class NewNoticiaPageModule {}
