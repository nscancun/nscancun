import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AngularFireStorage} from '@angular/fire/storage';
import { finalize, delay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AlertasService } from '../../services/alertas.service';
import { LoadingService } from '../../services/loading.service';


@Component({
  selector: 'app-new-noticia',
  templateUrl: './new-noticia.page.html',
  styleUrls: ['./new-noticia.page.scss'],
})
export class NewNoticiaPage implements OnInit {

  notiForm: FormGroup;
  noticia = {};

  cargaNoti = false;
  uploadPercent: Observable<number>;
  urlImage: Observable<string>;
  tasksRef: AngularFireList<any>;
  file: any;
  id: any;
  idimg: any;
  img;img_new;

  d = new Date();
  dia;mes;hora;minu;seg;

  constructor(
    private storage: AngularFireStorage,
    private database: AngularFireDatabase,
    private authSvc: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    public loadingController: LoadingController,
    private alertasSvc: AlertasService,
    private loadingSvc: LoadingService
  ) {
    this.notiForm = this.formBuilder.group({
      'titulo' : [null, Validators.required],
      'IMG' : [null],
      'noticia' : ['', Validators.required]
    });
  }

  ngOnInit() {
    this.tasksRef = this.database.list('/Noticias');
    this.idimg = Math.random().toString(36).substring(2);
  }
  // Sube la imagen a firebase
  subirIMG() {
    const filePath = 'noticias/noticia_' + this.idimg ;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload((filePath), this.file);

    this.uploadPercent = task.percentageChanges();
    this.loadingSvc.cargaIMG();
    task.snapshotChanges().pipe(
      finalize(() => {
        this.urlImage = ref.getDownloadURL();
        this.loadingSvc.dismiss();
      })).subscribe();
    console.log(this.urlImage);
    this.notiForm.controls['IMG'].setValue(this.urlImage);
  }

  // Obtine la imagen que abrimos
  onUpload(e) {
    this.notiForm.controls['IMG'].setValue('');
    this.file = e.target.files[0];
    console.log('subir', e.target.files[0]);
    this.subirIMG();
    this.previewImg(e);
  }

  // Se verifica la noticia para poder ser almacenada
  addNoticia(url: any) {
    this.fecha();
    this.id = 'm' + this.mes + '' + this.dia + '' + this.hora + '' + this.minu + '' + this.seg;

    const Ref = this.database.list('/Noticias/' + this.id);
    console.log('archivo: ', this.file);
    // Se verifica si el usuario a subido una imagen
    if (this.file) {
      this.img = url;
      this.guardarNoticia();
    } else {
      console.log('No hay: ', this.file);
      this.alertasSvc.errorSubirNoticiaAlert();
    }
  }

  // Guardar La noticia en la BD de Firebase
  guardarNoticia() {
      let currentUser = firebase.auth().currentUser;
      const t = this.notiForm.value.titulo;
      const n = this.notiForm.value.noticia;

      this.tasksRef.update(this.id, {
        // Igual aqui si sirve xds
        titulo: t,
        noticia: n,
        idUsuario: currentUser.uid,
        fecha: this.dia + '/' + this.mes,
        hora: this.hora,
        minuto: this.minu,
        urlIMG: this.img,
        idIMG: 'noticia_' + this.idimg
      });
      this.router.navigateByUrl('/ver/' + this.id);
      console.log('creado');
      this.alertasSvc.newNoticiaCorrectoAlert();

  }
  // obtenemos la fecha
  fecha() {
    this.dia = this.d.getDate();
    this.mes = this.d.getMonth() + 1;
    this.hora = this.d.getHours();
    this.minu = this.d.getMinutes();
    this.seg = this.d.getSeconds();
    if (this.minu >= 0 && this.minu <= 9 ) {
      this.minu = '0' + this.minu;
    }
    if (this.seg >= 0 && this.seg <= 9 ) {
      this.seg = '0' + this.seg;
    }
    if (this.mes >= 0 && this.mes <= 9 ) {
      this.mes = '0' + this.mes;
    }
    if (this.dia >= 0 && this.dia <= 9 ) {
      this.dia = '0' + this.dia;
    }
    console.log('dia', this.dia);
    console.log('mes', this.mes);
    console.log(this.mes, this.dia, this.hora, this.minu, this.seg);
  }

  // Muestra la imagen elegida al usuario
  previewImg(e) {
    let reader = new FileReader();
    reader.onload = (e:any) => {
      this.img_new = e.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
    }
}
