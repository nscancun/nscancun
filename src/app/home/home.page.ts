import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AuthService } from '../services/auth.service';
import { FirebaseServiceService } from '../services/firebase-service.service';
import * as firebase from 'Firebase';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  noticias: any;
  ref = firebase.database().ref('/Noticias');

  constructor(
    private authSvc: AuthService,
    public afd: AngularFireDatabase,
    private router: Router,
    private loadingSvc: LoadingService,
    ) {
      this.loadingSvc.cargando();
      this.ref.on('value', resp => {
        this.noticias = snapshotToArray(resp).reverse();
        console.log(this.noticias);
        this.loadingSvc.dismiss();
      });
      this.getPost();
    }

  verNoticia(key){
    console.log("click " + key);
    this.router.navigate(['/ver-Noticia/' + key]);
  }

  getPost() {
    const data = this.afd.list('/Noticias').valueChanges().subscribe(val => {
      console.log(val);
    });
  }

}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};


