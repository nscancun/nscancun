import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from '../shared/user.class';
import { AlertLoginService } from '../services/alert-login.service';

@Component({
  selector: 'app-loggin',
  templateUrl: './loggin.page.html',
  styleUrls: ['./loggin.page.scss'],
})
export class LogginPage implements OnInit {
  user: User = new User();

  constructor(
    public alertController: AlertController,
    private authSvc: AuthService,
    private router: Router,
    private al: AlertLoginService) { }

  ngOnInit() {
  }

  async AlertLogIn() {
    // const input = await this.alertController.create({
    const alert = await this.alertController.create({
      header: 'Iniciar Sesión',
      backdropDismiss: false,
      inputs: [
        {
          name: 'email',
          type: 'text',
          placeholder: 'Correo Electronico'
        },
        {
          name: 'password',
          type: 'password',
          placeholder: 'Contraseña'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ingresar',
          handler: data => {
            this.user.email = data.email;
            this.user.password = data.password;

            this.onLogin();
          }
        }
      ]
    });

    await alert.present();
    // await input.present();
  }

async AlertRegister() {
    // const input = await this.alertController.create({
    const alert = await this.alertController.create({
      header: 'Crear Cuenta',
      backdropDismiss: false,
      inputs: [
        {
          name: 'email',
          type: 'text',
          placeholder: 'Correo Electronico'
        },
        {
          name: 'password',
          type: 'password',
          placeholder: 'Contraseña'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Guardar',
          handler: data => {
            this.user.email = data.email;
            this.user.password = data.password;

            this.onRegister();
          }
        }
      ]
    });

    await alert.present();
    // await input.present();
  }
  async onLogin() {
    const user = await this.authSvc.onLogin(this.user);
    if (user) {
      console.log('Acceso correcto');
      this.al.accesoCorrectoAlert();
      this.router.navigateByUrl('/home');
    }
  }

  async onRegister() {
    const user = await this.authSvc.onRegister(this.user);
  }

  onLogout() {
    this.authSvc.logout();
    console.log('Adios');
  }
}
