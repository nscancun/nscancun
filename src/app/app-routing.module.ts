import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'loggin', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
    // canActivate: [AuthGuard]
  },
  {
    path: 'loggin',
    loadChildren: () => import('./loggin/loggin.module').then( m => m.LogginPageModule)
  },
  {
    path: 'perfil/:userid',
    loadChildren: () => import('./usuario/perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: 'new-noticia',
    loadChildren: () => import('./noticias/new-noticia/new-noticia.module').then( m => m.NewNoticiaPageModule)
  },
  {
    path: 'ver/:key',
    loadChildren: () => import('./noticias/ver/ver.module').then( m => m.VerPageModule)
  },
  {
    path: 'vernoticias',
    loadChildren: () => import('./usuario/vernoticias/vernoticias.module').then( m => m.VernoticiasPageModule)
  },
  {
    path: 'editar/:key',
    loadChildren: () => import('./noticias/editar/editar.module').then( m => m.EditarPageModule)
  },
  {
    path: 'editarperfil/:userid',
    loadChildren: () => import('./usuario/editarperfil/editarperfil.module').then( m => m.EditarperfilPageModule)
  },
  {
    path: 'favoritos',
    loadChildren: () => import('./usuario/favoritos/favoritos.module').then( m => m.FavoritosPageModule)
  },
  {
    path: 'notimexico',
    loadChildren: () => import('./noticias/notimexico/notimexico.module').then( m => m.NotimexicoPageModule)
  },
  {
    path: 'otrousuario/:userid',
    loadChildren: () => import('./otrousuario/otrousuario.module').then( m => m.OtrousuarioPageModule)
  },  {
    path: 'mapa',
    loadChildren: () => import('./mapa/mapa.module').then( m => m.MapaPageModule)
  },




];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
