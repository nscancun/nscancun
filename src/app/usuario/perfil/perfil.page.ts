import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../../services/auth.service';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  id;
  itemsRef: AngularFireList<any>;
  usuario = {};

  constructor(
    public alertController: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    public afd: AngularFireDatabase,
    private authSvc: AuthService,
    private loadingSvc: LoadingService,
  ) { }

  ngOnInit() {
    this.loadingSvc.cargando();
    this.id = this.route.snapshot.paramMap.get('userid');
    this.itemsRef = this.afd.list('/Usuarios');

    const data = this.afd.object('/Usuarios/' + this.id).valueChanges().subscribe(val => {
      this.usuario =  val;
      console.log(this.usuario);
      this.loadingSvc.dismiss();
    });
  }

  edit() {
    this.router.navigate(['/editarperfil/' + this.id]);
  }

  onLogout() {
    this.authSvc.logout();
    console.log('Adios');
  }
}
