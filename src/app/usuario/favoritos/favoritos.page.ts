import { Component, OnInit } from '@angular/core';
import * as firebase from 'Firebase';
import { LoadingService } from '../../services/loading.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.page.html',
  styleUrls: ['./favoritos.page.scss'],
})
export class FavoritosPage implements OnInit {

  noticias: any;
  fav: any;
  favUser = [];
  ref = firebase.database().ref('/Noticias');
  constructor(
    private loadingSvc: LoadingService,
    public afd: AngularFireDatabase,
  ) {
      this.loadingSvc.cargando();
      this.ref.on('value', resp => {
        this.noticias = snapshotToArray(resp);
        console.log(this.noticias);
        this.loadingSvc.dismiss();
      });
      this.getNoticias();
  }

  ngOnInit() {
  }

  getNoticias() {
    const data = this.afd.list('/Noticias').valueChanges().subscribe(val => {
      this.getFav();
    });
  }

  getFav() {
    let User = firebase.auth().currentUser;
    const favRef = this.afd.list('/Usuarios/' + User.uid + '/favoritos/').valueChanges().subscribe(val => {
      this.fav = val;
      for (const fav of this.fav) {
        for ( const noti of this.noticias) {
          if (fav.favId === noti.key) {
            this.favUser.push(noti);
            console.log('si');
          }
        }
      }
      console.log(this.fav);
      this.loadingSvc.dismiss();
    });
  }
}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};
