import { Component, OnInit } from '@angular/core';
import { finalize, delay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as firebase from 'firebase/app';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AlertasService } from '../../services/alertas.service';
import { LoadingService } from '../../services/loading.service';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-editarperfil',
  templateUrl: './editarperfil.page.html',
  styleUrls: ['./editarperfil.page.scss'],
})
export class EditarperfilPage implements OnInit {

  perfilForm: FormGroup;
  usersRef: AngularFireList<any>;
  img;img_new;img2;url;
  file: any;
  idUSER;
  idIMG;

  image: any;
  urlImage: Observable<string>;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    public afd: AngularFireDatabase,
    private formBuilder: FormBuilder,
    private alertasSvc: AlertasService,
    private loadingSvc: LoadingService,
    private storage: AngularFireStorage,
    private database: AngularFireDatabase,
  ) {
    this.perfilForm = this.formBuilder.group({
      'Nombre' : [null, Validators.required],
      'Apellido' : [null, Validators.required],
      'Tel' : [null], 'descr' : [null], 'IMG' : [null]
    });
  }

  ngOnInit() {
    this.idUSER = this.route.snapshot.paramMap.get('userid');
    this.getInfo();
    this.img2 = this.img;
    this.usersRef = this.database.list('/Usuarios');
  }
  // Obtine la informacion del usario y la coloca en los campos a editar
  getInfo() {
    firebase.database().ref('Usuarios/' + this.idUSER).on('value', resp => {
      let infoUsuario = snapshotToObject(resp);
      this.perfilForm.controls['Nombre'].setValue(infoUsuario.Nombre);
      this.perfilForm.controls['Apellido'].setValue(infoUsuario.Apellidos);
      this.perfilForm.controls['Tel'].setValue(infoUsuario.Tel);
      this.perfilForm.controls['descr'].setValue(infoUsuario.Descripcion);
      this.img = infoUsuario.photoURL;
    });
  }

  // Obtine la imagen que abrimos
  onUpload(e) {
    this.perfilForm.controls['IMG'].setValue('');
    this.file = e.target.files[0];
    this.subirIMG();
    this.previewImg(e);
    console.log('subir', e.target.files[0]);
  }
  // Muestra la imagen elegida al usuario
  previewImg(e) {
    let reader = new FileReader();
    reader.onload = (e:any) => {
      this.img_new = e.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
    this.img2 = '';
  }

  // Sube la imagen a firebase
  subirIMG() {
    const filePath = 'usuarios/perfil_' + this.idUSER + this.idIMG ;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload((filePath), this.file);
    if (this.idIMG) {
      this.loadingSvc.cargaIMG();
    } else {
      this.loadingSvc.cargaNoti();
    }
    task.snapshotChanges().pipe(
      finalize(() => {
        this.urlImage = ref.getDownloadURL();
        this.loadingSvc.dismiss();
      })).subscribe();
    console.log('url img', this.urlImage);
    this.perfilForm.controls['IMG'].setValue(this.urlImage);
  }

  //verifica si existe una imagen
  verificar(url: any) {
    this.idIMG = this.idUSER;
    if (this.file) {
      this.subirIMG();
      this.img = url;
      this.updateUser();
    } else {
      this.updateUser();
    }
  }
  // Actualizar informacion de usuario
  updateUser() {
    const nombre = this.perfilForm.value.Nombre;
    const apellido = this.perfilForm.value.Apellido;
    let tel = this.perfilForm.value.Tel;
    let des = this.perfilForm.value.descr;
    if (!tel) {
      tel = '';
    }
    if (!des) {
      des = '';
    }
    this.usersRef.update(this.idUSER, {
      Nombre: nombre,
      Apellidos: apellido,
      Tel: tel,
      Descripcion: des,
      photoURL: this.img
    });
    // this.router.navigateByUrl('/perfil/' + this.idUSER);
    this.url = '/perfil/' + this.idUSER;
    console.log('creado');
    this.alertasSvc.updateUserAlert();
  }
}

export const snapshotToObject = snapshot => {
  let item = snapshot.val();
  item.key = snapshot.key;

  return item;
};
