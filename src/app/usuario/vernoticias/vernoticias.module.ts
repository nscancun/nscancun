import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VernoticiasPageRoutingModule } from './vernoticias-routing.module';

import { VernoticiasPage } from './vernoticias.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VernoticiasPageRoutingModule,
    ComponentsModule
  ],
  declarations: [VernoticiasPage]
})
export class VernoticiasPageModule {}
