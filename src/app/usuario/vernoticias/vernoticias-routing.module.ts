import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VernoticiasPage } from './vernoticias.page';

const routes: Routes = [
  {
    path: '',
    component: VernoticiasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VernoticiasPageRoutingModule {}
