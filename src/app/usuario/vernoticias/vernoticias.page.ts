import { AuthService } from '../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-vernoticias',
  templateUrl: './vernoticias.page.html',
  styleUrls: ['./vernoticias.page.scss'],
})
export class VernoticiasPage implements OnInit {

  public isLogged: any = false;
  noticias: any;
  noticiasUsua = [];
  itemsRef;

  infos = [];
  ref = firebase.database().ref('Noticias/');

  constructor(
    private authSvc: AuthService,
    public afd: AngularFireDatabase,
    private router: Router,
    private loadingSvc: LoadingService,
    public alertController: AlertController
  ) {
    this.ref.on('value', resp => {
      this.infos = [];
      this.infos = snapshotToArray(resp).reverse();
    });
   }

  ngOnInit() {
    this.loadingSvc.cargando();
    this.itemsRef = this.afd.list('/Noticias');
    this.obtenerTodas();
  }
  // Obtener Las noticias del usario
  noticiUsa() {
    this.noticiasUsua = [];
    let currentUser = firebase.auth().currentUser;
    for (const noti of this.infos) {
      if (noti.idUsuario === currentUser.uid) {
        this.noticiasUsua.push(noti);
      }
    }
    console.log('Noticias Usuario ', this.noticiasUsua);
  }

  edit(key) {
    this.router.navigate(['/editar/' + key]);
  }

  async delete(key) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: '¿Estas seguro de eliminar esta noticia?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('cancel');
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            this.itemsRef.remove(key);
            console.log('eliminado', key);
          }
        }
      ]
    });
    await alert.present();
  }

  obtenerTodas() {
    this.afd.list('/Noticias').valueChanges().subscribe(val => {
      if (val) {
        this.noticias = val;
        console.log(val);
        this.noticiUsa();
        this.loadingSvc.dismiss();
      }
      else {
      }
    });
  }
}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};
