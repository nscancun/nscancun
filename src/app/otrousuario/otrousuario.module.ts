import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtrousuarioPageRoutingModule } from './otrousuario-routing.module';

import { OtrousuarioPage } from './otrousuario.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OtrousuarioPageRoutingModule,
    ComponentsModule
  ],
  declarations: [OtrousuarioPage]
})
export class OtrousuarioPageModule {}
