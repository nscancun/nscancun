import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import * as firebase from 'firebase/app';
import { LoadingService } from '../services/loading.service';

@Component({
  selector: 'app-otrousuario',
  templateUrl: './otrousuario.page.html',
  styleUrls: ['./otrousuario.page.scss'],
})
export class OtrousuarioPage implements OnInit {

  id;
  itemsRef: AngularFireList<any>;
  usuario = {};
  noticias: any;
  noticiasUsua = [];

  infos = [];
  ref = firebase.database().ref('Noticias/');

  constructor(
    public alertController: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    public afd: AngularFireDatabase,
    private authSvc: AuthService,
    private loadingSvc: LoadingService,
  ) {
    this.ref.on('value', resp => {
      this.infos = [];
      this.infos = snapshotToArray(resp);
    });
  }

  ngOnInit() {
    this.loadingSvc.cargando();
    this.id = this.route.snapshot.paramMap.get('userid');
    this.itemsRef = this.afd.list('/Usuarios');

    const data = this.afd.object('/Usuarios/' + this.id).valueChanges().subscribe(val => {
      this.usuario =  val;
      console.log(this.usuario);
      this.loadingSvc.dismiss();
    });

    this.obtenerTodas();
  }

  obtenerTodas() {
    this.afd.list('/Noticias').valueChanges().subscribe(val => {
      if (val) {
        this.noticias = val;
        console.log(val);
        this.noticiUsa();
        this.loadingSvc.dismiss();
      }
      else {
      }
    });
  }

  // Obtener Las noticias del usario
  noticiUsa() {
    this.noticiasUsua = [];
    for (const noti of this.infos) {
      if (noti.idUsuario === this.id) {
        this.noticiasUsua.push(noti);
      }
    }
    console.log('Noticias Usuario ', this.noticiasUsua);
  }
}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};
