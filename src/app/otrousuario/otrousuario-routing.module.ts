import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtrousuarioPage } from './otrousuario.page';

const routes: Routes = [
  {
    path: '',
    component: OtrousuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtrousuarioPageRoutingModule {}
