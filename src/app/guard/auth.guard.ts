import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { AlertasService } from '../services/alertas.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authSvc: AuthService,
    private router: Router,
    private alertSvc: AlertasService
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean | UrlTree {
      // return false;
      if (this.authSvc.isLogged) {
        return true;
      }
      console.log('Accesos denegado!');
      this.alertSvc.errorSesionAlert();
      this.router.navigateByUrl('/loggin');
      return false;
    }
}
