import { Component, OnInit } from '@angular/core';
import * as firebase from 'Firebase';
import { AngularFireDatabase } from 'angularfire2/database';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})
export class MapaPage implements OnInit {

  noticias: any;
  ref = firebase.database().ref('/Noticias');

  constructor(
    public afd: AngularFireDatabase,
    private router: Router,
    private loadingSvc: LoadingService,
    ) {
      this.loadingSvc.cargando();
      this.ref.on('value', resp => {
        this.noticias = snapshotToArray(resp);
        console.log(this.noticias);
        this.loadingSvc.dismiss();
      });
      this.getPost();
    }

  ngOnInit() {
  }

  verNoticia(key){
    console.log("click " + key);
    this.router.navigate(['/ver-Noticia/' + key]);
  }

  getPost() {
    const data = this.afd.list('/Noticias').valueChanges().subscribe(val => {
      console.log(val);
      // this.verMapa();
    });
  }

  // MAPÄ
  verMapa() {
    // var dii = document.getElementById('map');
    // const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
    // mapboxgl.accessToken = 'pk.eyJ1IjoibWVnYWRyZXMiLCJhIjoiY2sxcnR1MGw4MDh6ZDNtcWNteDhuZ282eiJ9.2Aw2Pz5KsmPlZ9oO4NAxgg';

    // var map = new mapboxgl.Map({
    // container: 'map',
    // center: [-86.8420, 21.15233], // starting position
    // style: 'mapbox://styles/mapbox/outdoors-v11',
    // zoom: 11
    // });
    // map.addControl(new mapboxgl.FullscreenControl());
    // map.addControl(new mapboxgl.NavigationControl());

    //   var popup = new mapboxgl.Popup({ closeOnClick: false })
    //   .setLngLat([-86.8420, 21.15233])
    //   .setHTML('<h5>Cancun</h5>')
    //   .addTo(map);
    // create the popup
    // for (const noti of this.noticias) {
    //   // var popup = new mapboxgl.Popup({ offset: 25 }).setText(
    //   //   '{{noti.titulo}}'
    //   //   );

    //   var el = document.getElementById(noti.key);
    //   new mapboxgl.Marker(el)
    //   .setLngLat([noti.x, noti.y])
    //   // .setPopup(popup) // sets a popup on this marker
    //   .addTo(map);
    // }

    // create DOM element for the marker
    // var el = document.getElementById('marker');
    // // el.id = 'marker';

    // // create the marker
    // new mapboxgl.Marker(el)
    // .setLngLat([-86.8420, 21.15233])
    // .setPopup(popup) // sets a popup on this marker
    // .addTo(map);
  }
}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};
